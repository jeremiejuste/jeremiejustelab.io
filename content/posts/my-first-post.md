+++
title = "Address matching"
author = ["Jeremie Juste"]
date = 2024-04-18T16:24:00+02:00
tags = ["nlp"]
categories = ["DataScience"]
draft = false
+++

Address matching — the process of identifying pairs of address records
referring to the same spatial footprint — is increasingly required for
enriching data quality in a wide range of real-world applications. [reference](https://medium.com/streetgroup/a-street-group-address-matching-algorithm-e7e1444767cd)


## [pypostal](https://github.com/openvenues/pypostal) {#pypostal}

A module to parse postal address


## dependencies {#dependencies}

```bash
sudo apt-get install curl autoconf automake libtool python-is-python3 pkg-config
```


### install libpostal {#install-libpostal}

```bash
git clone https://github.com/openvenues/libpostal
cd libpostal
./bootstrap.sh
./configure --datadir=[...some dir with a few GB of space...]
make
sudo make install

# On Linux it's probably a good idea to run
sudo ldconfig
```

```bash
git clone https://github.com/openvenues/libpostal
```

This is my first post body
