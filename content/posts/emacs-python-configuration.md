+++
title = "My python configuration on emacs (again)"
date = 2024-04-29T16:48:00+02:00
tags = ["python"]
draft = false
+++

## Python configuration again {#python-configuration-again}

I have configured python many times in emacs as I keep losing them. In an attempt to have a faster setup I will document the process to install python and setting up the emacs to have usable python environment. To do that we need to setup an environment from scratch. A debian container is ideal for that. I am using [podman](https://podman.io/) instead of docker and I am using the official debian container from dockerhub.

Note that this blog post is not static and I will modify/improve it as new insights are discovered.


## Get a new instance of debina {#get-a-new-instance-of-debina}

```bash
podman search docker.io/debian --filter=is-official
```

NAME                           DESCRIPTION
docker.io/library/ubuntu       Ubuntu is a Debian-based Linux operating sys...
docker.io/library/debian       Debian is a Linux distribution that's compos...
docker.io/library/neurodebian  NeuroDebian provides neuroscience research s...

```bash
podman pull docker.io/library/debian
```

```text
c978d997d5fe02d50ae8b7c1e4338f3fcdb61bcf9940a8ce8f87811a319ce586
```

```bash
podman images
```

```text
REPOSITORY                TAG         IMAGE ID      CREATED      SIZE
docker.io/library/debian  latest      c978d997d5fe  4 weeks ago  121 MB
docker.io/library/nginx   latest      92b11f67642b  7 weeks ago  191 MB
```

```bash
podman run --name test_python  -dt -p 8080:80/tcp docker.io/library/debian
```

```text
e9b208b265a3fd033377fdc5eb2b8be20520c242ed4bf0ae5fd6fd6ff5d09da7
```

```text
c925c22b7b8a
```

The image is running

```bash
podman ps -a
```

```text
CONTAINER ID  IMAGE                            COMMAND     CREATED        STATUS            PORTS                 NAMES
e9b208b265a3  docker.io/library/debian:latest  bash        3 seconds ago  Up 3 seconds ago  0.0.0.0:8080->80/tcp  test_python
```

```bash
podman start e9b208b265a3
```

```text
e9b208b265a3
```


## Install python {#install-python}

```bash
apt update
apt install python3
apt install python3.11-venv
apt install emacs
```

We need to install python venv as it is a separate module from python in the debian repository


## creating an environment {#creating-an-environment}

```elisp
(pyvenv-create "default" "/usr/bin/python3")
```


## install ipython {#install-ipython}

```bash

source ~/.virtualenvs/default/bin/activate && pip install ipython
```

source is not available on the system

So to install ipython one can use this alternative path.

```bash

cd ~/.virtualenvs/default/bin/ && ./pip install ipython
```

Next let's start configuring emacs.


## Emacs Configuration {#emacs-configuration}

```elisp
File Edit Options Buffers Tools Emacs-Lisp Virtual Envs Help
(require 'package)
(add-to-list 'package-archives
             '("melpau" . "https://melpa.org/packages/") t)
(package-refresh-contents)
(package-install 'use-package)

(set-register ?a '(file . "~/.emacs.d/init.el"))


(package-install 'lsp-jedi)
(use-package lsp-jedi
  :ensure t)

(setq completion-styles '(flex basic partial-completion emacs22))



(use-package eglot
  :ensure t
  :hook (python-mode . eglot-ensure))


(use-package pyvenv
  :ensure t)

(use-package python-black
  :ensure t)

(setq tab-always-indent 'complete)
(setq completion-styles '(flex basic partial-completion emacs22))


(use-package python
  :init
 (setq python-shell-interpreter "ipython"
       python-shell-interpreter-args "-i --simple-prompt --InteractiveShell.display_page=True")
 (pyvenv-activate "~/.virtualenvs/default")

:config
 (setq python-indent-guess-indent-offset nil)
:hook
((python-mode . pyvenv-mode))
(python-mode . python-black-on-save-mode)
)
```

The blog post of [Robert Enzmann](https://robbmann.io/posts/006_emacs_2_python/) is really insightful regarding the capabilities of the vanilla python mode. Coming from R ESS it has always been a struggle to evaluate regions in python smoothly. Thanks to this post I have rediscovered the expected ways of evaluating expressions and for any future customization, I will build on the plain vanilla functions.
