+++
title = "Home page"
author = ["Jeremie Juste"]
date = 2022-09-05T00:30:00+02:00
lastmod = 2023-10-17T22:11:00+02:00
draft = false
+++

<div class="lead">

> Yeah, well, I'm going to go build my own site. <br />
> With Emacs!  And Nix! <br />
> In fact, forget the site.
</div>

<!--more-->
