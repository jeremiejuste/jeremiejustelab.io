+++
title = "About"
date = 2024-04-20T21:53:00+02:00
draft = false
+++

Hello I'm Jérémie, a data scientist with extensive training econometrics. With a passion for using data to drive insights and inform business decisions and helping organizations like yours solve complex problems and achieve their goals.

As a data scientist, I specialize in the development of credit risk tools, and have experience working with a variety of tools and technologies, including R and Python. My expertise includes statistical analysis and data visualization, and I'm always looking to expand my knowledge and skills in the field.
