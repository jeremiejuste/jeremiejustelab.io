+++
title = "Interesting blogs"
date = 2024-04-29
draft = true
+++

This is a post to reference blogs and resources that I find interesting on the internet.


## Blogs on Python {#blogs-on-python}

-   [Robert Enzmann](https://robbmann.io/)
