#+options: ^t:nil  author:nil
#+hugo_base_dir: ../
#+author: Jeremie Juste


* DONE Home page
	CLOSED: [2022-09-05 Mon 00:30 EDT]
	:PROPERTIES:
	:EXPORT_FILE_NAME: _index
	:EXPORT_HUGO_SECTION: /
	:EXPORT_HUGO_LASTMOD: [2023-10-17 Tue 22:11 EDT]
	:END:


* DONE About                                                          :about:
CLOSED: [2024-04-20 Sat 21:53]
:PROPERTIES:
:EXPORT_HUGO_SECTION: /
:EXPORT_FILE_NAME: _index
:EXPORT_DATE: 2024-02-20
:END:
Hello I'm Jérémie, a data scientist with extensive training econometrics. With a passion for using data to drive insights and inform business decisions and helping organizations like yours solve complex problems and achieve their goals.

As a data scientist, I specialize in the development of credit risk tools, and have experience working with a variety of tools and technologies, including R and Python. My expertise includes statistical analysis and data visualization, and I'm always looking to expand my knowledge and skills in the field. 


* DONE Address matching                                     :nlp:@DataScience:
CLOSED: [2024-04-18 Thu 16:24]
:PROPERTIES:
:EXPORT_FILE_NAME: my-first-post
:EXPORT_HUGO_SECTION: posts
:END:


Address matching — the process of identifying pairs of address records
referring to the same spatial footprint — is increasingly required for
enriching data quality in a wide range of real-world applications. [[https://medium.com/streetgroup/a-street-group-address-matching-algorithm-e7e1444767cd][reference]]

** [[https://github.com/openvenues/pypostal][pypostal]]
A module to parse postal address

** dependencies
#+begin_src bash :results output
sudo apt-get install curl autoconf automake libtool python-is-python3 pkg-config
#+end_src

*** install libpostal
#+begin_src bash :results output :eval no
git clone https://github.com/openvenues/libpostal
cd libpostal
./bootstrap.sh
./configure --datadir=[...some dir with a few GB of space...]
make
sudo make install

# On Linux it's probably a good idea to run
sudo ldconfig
#+end_src


#+begin_src bash :results output :dir ~/Documents/git-repo
git clone https://github.com/openvenues/libpostal
#+end_src


This is my first post body




* DONE Greatest common divisor                                 :drill:python:
:PROPERTIES:
:EXPORT_FILE_NAME: greatest_common_divisor
:EXPORT_HUGO_SECTION: posts
:EXPORT_DATE: 2024-02-20
:author:
:END:

CLOSED: [2024-04-18 Thu 23:34]

This is a classical question in coding interview. I did it countless number of times on paper but didn't know the Euler algorithm for solving it.

let a and b be two integers. The greatest common divisor of a and b
#+begin_quote
*repeat*
1. a % b = R
2. a=b and b = R
*until R = 0*

gcd =  b
#+end_quote

#+begin_src python :results value :exports both
  def gcd(a: int,b: int):
      """Use the Euler algorithm to find the gcd between 2 integers."""
      R = a % b
      if R == 0:
	  return b
      else:
	  return gcd(b,R)

  return gcd(24,36)	  
#+end_src

#+RESULTS:
: 12

 



* TODO Interesting blogs
:PROPERTIES:
:EXPORT_FILE_NAME: _index
:EXPORT_HUGO_SECTION: useful-blogs
:EXPORT_DATE: 2024-04-29
:END:
This is a post to reference blogs and resources that I find interesting on the internet.

** Blogs on Python
- [[https://robbmann.io/][Robert Enzmann]]

* DONE My python configuration on emacs (again)                      :python:
CLOSED: [2024-04-29 Mon 16:48]
:PROPERTIES:
:EXPORT_FILE_NAME: emacs-python-configuration
:EXPORT_HUGO_SECTION: posts
:EXPORT_DATE: {{{date}}}
:header-args:bash:    :eval no :exports both
:END:

** Python configuration again
I have configured python many times in emacs as I keep losing them. In an attempt to have a faster setup I will document the process to install python and setting up the emacs to have usable python environment. To do that we need to setup an environment from scratch. A debian container is ideal for that. I am using [[https://podman.io/][podman]] instead of docker and I am using the official debian container from dockerhub.

Note that this blog post is not static and I will modify/improve it as new insights are discovered.


** Get a new instance of debina

#+begin_src bash :results output raw
  podman search docker.io/debian --filter=is-official
#+end_src

#+RESULTS:
NAME                           DESCRIPTION
docker.io/library/ubuntu       Ubuntu is a Debian-based Linux operating sys...
docker.io/library/debian       Debian is a Linux distribution that's compos...
docker.io/library/neurodebian  NeuroDebian provides neuroscience research s...

#+begin_src bash :results output
podman pull docker.io/library/debian
#+end_src

#+RESULTS:
: c978d997d5fe02d50ae8b7c1e4338f3fcdb61bcf9940a8ce8f87811a319ce586

#+begin_src bash :results output
podman images
#+end_src

#+RESULTS:
: REPOSITORY                TAG         IMAGE ID      CREATED      SIZE
: docker.io/library/debian  latest      c978d997d5fe  4 weeks ago  121 MB
: docker.io/library/nginx   latest      92b11f67642b  7 weeks ago  191 MB

#+begin_src bash :results output
podman run --name test_python  -dt -p 8080:80/tcp docker.io/library/debian
#+end_src

#+RESULTS:
: e9b208b265a3fd033377fdc5eb2b8be20520c242ed4bf0ae5fd6fd6ff5d09da7


#+RESULTS:
: c925c22b7b8a

The image is running
#+begin_src bash :results output
podman ps -a
#+end_src

#+RESULTS:
: CONTAINER ID  IMAGE                            COMMAND     CREATED        STATUS            PORTS                 NAMES
: e9b208b265a3  docker.io/library/debian:latest  bash        3 seconds ago  Up 3 seconds ago  0.0.0.0:8080->80/tcp  test_python


#+begin_src bash :results output :session *shared*
podman start e9b208b265a3
#+end_src

#+RESULTS:
: e9b208b265a3


** Install python

#+begin_src bash :results output
  apt update
  apt install python3
  apt install python3.11-venv
  apt install emacs
#+end_src

We need to install python venv as it is a separate module from python in the debian repository

** creating an environment

#+begin_src elisp
(pyvenv-create "default" "/usr/bin/python3")
#+end_src

** install ipython

#+begin_src bash

  source ~/.virtualenvs/default/bin/activate && pip install ipython
#+end_src
source is not available on the system

So to install ipython one can use this alternative path.

#+begin_src bash

  cd ~/.virtualenvs/default/bin/ && ./pip install ipython
#+end_src

Next let's start configuring emacs.

** Emacs Configuration

#+begin_src elisp
File Edit Options Buffers Tools Emacs-Lisp Virtual Envs Help                                                                                                  
(require 'package)
(add-to-list 'package-archives
             '("melpau" . "https://melpa.org/packages/") t)
(package-refresh-contents)
(package-install 'use-package)

(set-register ?a '(file . "~/.emacs.d/init.el"))


(package-install 'lsp-jedi)
(use-package lsp-jedi
  :ensure t)

(setq completion-styles '(flex basic partial-completion emacs22))



(use-package eglot
  :ensure t
  :hook (python-mode . eglot-ensure))


(use-package pyvenv
  :ensure t)

(use-package python-black
  :ensure t)

(setq tab-always-indent 'complete)
(setq completion-styles '(flex basic partial-completion emacs22))


(use-package python
  :init
 (setq python-shell-interpreter "ipython"
       python-shell-interpreter-args "-i --simple-prompt --InteractiveShell.display_page=True")
 (pyvenv-activate "~/.virtualenvs/default")

:config
 (setq python-indent-guess-indent-offset nil)
:hook
((python-mode . pyvenv-mode))
(python-mode . python-black-on-save-mode)
)

#+end_src

The blog post of [[https://robbmann.io/posts/006_emacs_2_python/][Robert Enzmann]] is really insightful regarding the capabilities of the vanilla python mode. Coming from R ESS it has always been a struggle to evaluate regions in python smoothly. Thanks to this post I have rediscovered the expected ways of evaluating expressions and for any future customization, I will build on the plain vanilla functions.


* COMMENT Local Variables                          :ARCHIVE:
# Local Variables:
# eval: (org-hugo-auto-export-mode)
# End:
